CONNECT 'jdbc:derby:MurachDB;create=true';

-- drop Customers table (ignore errors if it doesn't exist)
DROP TABLE Customers;

-- create Customers table
CREATE TABLE Customers
(
    nameFirst VARCHAR(20), 
	nameLast VARCHAR(20), 
	phoneNumber VARCHAR (12),
	email VARCHAR (30)
);

-- insert data into Customers table
INSERT INTO Customers VALUES
('Horatio', 'Nelson', '123-456-7890', 'hnelson@admiral.uk');   	

INSERT INTO Customers VALUES
('Neil', 'Fallon', '555.696.3548', 'lsinger@clutch.com');

INSERT INTO Customers VALUES
('Jean', 'Gastier', '545.689.1234', 'drummer@clutch.com');

INSERT INTO Customers VALUES
('Dan', 'Maines', '566.879.5454', 'iplaybass@clutch.com');

INSERT INTO Customers VALUES
('Tim', 'Sult', '258.369.3654', 'iruleax@clutch.com');


-- view data in Customers table
SELECT * FROM Customers;

DISCONNECT;