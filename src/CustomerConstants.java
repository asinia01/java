/*
 * declares constants to use with the string utils class
 */

/**
 *
 * @author Andrew M. Siniarski
 *          the dude abides
 * @created 4 Jun 15
 */
public interface CustomerConstants 
{
    int EMAIL_SIZE = 25;
    int FNAME_SIZE = 10;
    int LNAME_SIZE = 15;
    int PHONE_NUMBER_SIZE = 12;
}
