/*
    Interface for abstract methods
        addCustomer, deleteCustomer, updateCustomer
 */

/**
 *
 * @author Andrew M. Siniarski
 *          the dude abides
 */
public interface CustomerWriter 
{
    boolean addCustomer(Customer c);
    boolean deleteCustomer(Customer c);
    boolean updateCustomer(Customer c);
}
