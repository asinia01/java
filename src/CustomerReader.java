/*
    Interface for abstract method getCustomer and ArrayList
 */

/**
 *
 * @author Andrew M. Siniarski
 *          the dude abides
 */

import java.util.ArrayList;

public interface CustomerReader 
{
    Customer getCustomer(String email);
    ArrayList<Customer> getCustomers();
}
