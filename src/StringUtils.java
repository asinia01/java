/*
    Class to format everything so that it looks nice
 */

/**
 *
 * @author Andrew M. Siniarski
 *          the dude abides
 */
public class StringUtils
{
    public static String padWithSpaces(String s, int length)
    {
        if (s.length() < length)
        {
            StringBuilder sb = new StringBuilder(s);
            while(sb.length() < length)
            {
                sb.append(" ");
            }
            return sb.toString();
        }
        else
        {
            return s.substring(0, length);
        }
    }
}