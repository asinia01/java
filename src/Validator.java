/*
 * class to validate all input
 */

/**
 *
 * @author Andrew M. Siniarski
 *          the dude abides
 * @created 2 June 15
 */

import java.util.regex.*;
import java.util.Scanner;
import java.lang.String;

public class Validator 
{
    //method to get the entire line
    public static String getLine(Scanner sc, String prompt)
    {
        System.out.print(prompt);
        String s = sc.nextLine();   //reads the entire line
        s.trim();   //make sure there's no extra spaces
        return s;
    }
    
    //method to get the first string on the line
    public static String getString(Scanner sc, String prompt)
    {
        System.out.print(prompt);
        String s = sc.next();   //reads the first string on the line
        s.trim(); //make sure there's no extra spaces
        return s;               // discards the rest
    }
    
    //method to validate email
    public static boolean isEmail(String email)
    {
        String eTest =
          "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\\\[[0-9]{1,3}\\\\.[0-9]{1,3}" +
          "\\\\.[0-9]{1,3}\\\\.[0-9]{1,3}\\\\])|(([a-zA-Z\\\\-0-9]+\\\\.)" +
                "+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(eTest);
        java.util.regex.Matcher m = p.matcher(email);
        if(!m.matches())
        {
            System.out.println("That is not a valid email address" +
                                "Please try again");
            return false;
        }
        else
            return true;
    }
    
    //method to validate the phone number
    public static boolean isPhoneNumber(String phoneNumber)
    {
        boolean isValid = false;
        //store the first and second break character
        char firstBreak = phoneNumber.charAt(3);
        char lastBreak = phoneNumber.charAt(7);
        //regex to check if numeric
        String areaCodeTest = "^[1-9]{3}$";
        String threeNumTest = "^[1-9]{3}$";
        String fourNumTest = "^[0-9]{4}$";
        
        //make sure that it's in the correct format, with three numbers
        //three numbers four numbers, separated by - or .
        if(phoneNumber.length() == 12 && (firstBreak == '-' || firstBreak == '.')
            && (lastBreak == '-' || lastBreak == '.'))
        {
            //explode the string into three different strings
            String areaCode = phoneNumber.substring(0, 3);
            String firstThreeNum = phoneNumber.substring(4,7);
            String lastFourNum = phoneNumber.substring(8);
            //regex stuff

            if(areaCode.matches(areaCodeTest) && 
                    firstThreeNum.matches(threeNumTest) && 
                    lastFourNum.matches(fourNumTest))
            {
                isValid = true;
            }
        }

        
        return isValid;
        
    }
}
