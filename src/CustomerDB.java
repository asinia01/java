


/*
    Class that implements the CustomerRW interface
    this class handles all of the db, SQL transactions
 */

/**
 *
 * @author Andrew M. Siniarski
 *          the dude abides
 */

//necessary imports 
import java.sql.*;
import java.util.*;


public class CustomerDB implements CustomerRW
{
    //connect to the Derby db
    private Connection getConnection()
    {
        
        Connection connection = null;
        try
        {
            
            // if necessary, set the home directory for Derby
            String dbDirectory = "c:/murach/java/db";
            System.setProperty("derby.system.home", dbDirectory);

            // set the db url, username, and password
            String url = "jdbc:derby:MurachDB;create=true";
            String username = "";
            String password = "";

            connection = DriverManager.getConnection(url, username, password);
            return connection;
        }
        catch(SQLException e)
        {
            System.err.println(e);
            return null;
        }
    }
    
    //disconnect from the derby db
    public boolean disconnect()
    {
        try
        {
            //On successful shutdown, this throws a SQLException
            String shutdownURL = "jdbc:derby:;shutdown=true";
            DriverManager.getConnection(shutdownURL);
        }
        catch(SQLException e)
        {
            //if the error is a shutdown message, return true
            if(e.getMessage().equals("Derby system shutdown."))
            {
                System.out.println("Derby Database now disconnected");
                return true;
            }
        }
        
        //else return false
        return false;
    }
    
    //method to use for the list command
    //will get all the customers in the db
    public ArrayList<Customer> getCustomers()
    {
        String sql = "SELECT nameFirst, nameLast, phoneNumber, email "
                    + "FROM Customers ORDER BY nameLast ASC";
        ArrayList<Customer> customers = new ArrayList<>();
        
        try(Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery())
        {
            while(rs.next())
            {
                String nameFirst = rs.getString("nameFirst");
                String nameLast = rs.getString("nameLast");
                String phoneNumber = rs.getString("phoneNumber");
                String email = rs.getString("email");
                
                Customer c = new Customer(nameFirst, nameLast, phoneNumber, email);
                customers.add(c);
            }
            
            return customers;
        }
        catch(SQLException e)
        {
            System.err.println(e);
            return null;
        }
    }
    
    //method to return a single customer, based on email
    public Customer getCustomer(String email)
    {
        String sql =
             "SELECT nameFirst, nameLast, phoneNumber, email " +
             "FROM Customers WHERE email = ?";
        try(Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(sql))
        {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                String nameFirst = rs.getString("nameFirst");
                String nameLast = rs.getString("nameLast");
                String phoneNumber = rs.getString("phoneNumber");
                Customer c = new Customer(nameFirst, nameLast, phoneNumber, email);
                rs.close();
                return c;
            }
            else
            {
                rs.close();
                return null;
            }
        }
        catch(SQLException e)
        {
            System.err.println(e);
            return null;
        }
    }
    
    //method to add a new customer object
    public boolean addCustomer(Customer c)
    {
        String sql =
            "INSERT INTO Customers(nameFirst, nameLast, phoneNumber, email " +
            "VALUES(?, ?, ?, ?)";
        try(Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(sql))
        {
            ps.setString(1, c.getNameFirst());
            ps.setString(2, c.getNameLast());
            ps.setString(3, c.getPhoneNumber());
            ps.setString(4, c.getEmail());
            ps.executeUpdate();
            return true;
        }
        catch(SQLException e)
        {
            System.err.println(e);
            return false;
        }
        
    }
    
    //method to delete a customer object
    public boolean deleteCustomer(Customer c)
    {
        String sql = "DELETE FROM Customers " +
                      "WHERE email = ?";
        try(Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(sql))
        {
            ps.setString(1, c.getEmail());
            ps.executeUpdate();
            return true;
        }
        catch(SQLException e)
        {
            System.err.println(e);
            return false;
        }
    }
    
    //update a customer based on email
    public boolean updateCustomer(Customer c)
    {
        String sql = "UPDATE Customers SET " +
                     "nameFirst = ?, nameLast = ?, phoneNumber = ? " +
                     "WHERE email = ?";
        try(Connection connection = getConnection();
            PreparedStatement ps = connection.prepareStatement(sql))
        {
            ps.setString(1, c.getNameFirst());
            ps.setString(2, c.getNameLast());
            ps.setString(3, c.getPhoneNumber());
            ps.setString(4, c.getEmail());
            ps.executeUpdate();
            return true;
        }
        catch(SQLException e)
        {
            System.err.println(e);
            return false;
        }
    }
}
