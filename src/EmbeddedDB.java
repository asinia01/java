/*
 * @author: Andrew M. Siniarsk
            the dude abides
   @created: 1 Jun 15
    this is an interface to enable the CustomerRW to call the disconnect()
    method of the CustomerDB class
 */

/**
 *
 * @author Home
 */
public interface EmbeddedDB {
    
   boolean disconnect();
}
