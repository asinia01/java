/*
 * @AUTHOR: Andrew M. Siniarski
            the dude abides
   @started on: 2 Jun 15
   @finished on:
    
   this application will connect to a Derby db and maintain a customer list
    that consists of at least three fields: email, last and first name

 */

//necessary imports
import java.util.Scanner;
import java.util.ArrayList;

public class CustomerMaintApp implements CustomerConstants
{
    //declare two class variables
    private static CustomerRW customerRW = null;
    private static Scanner sc = null;
    
    public static void main(String args[])
    {
        //display a welcome message
        System.out.println("Welcome to The Dude's Customer Maintenance" +
                            "Application");
        System.out.println("******************************************" +
                            "***********");
        
        //set class variables
        customerRW = MakeRW.getCustomerRW();
        sc = new Scanner(System.in);
        
        //display the menu
        displayMenu();
        
        //prime the while loop
        String action = "";
        while(!action.equalsIgnoreCase("exit"))
        {
            //get input from user
            action = Validator.getString(sc, "Enter a command: ");
            //menu options that will send to appropriate method
            if(action.equalsIgnoreCase("list"))
                displayAllCustomers();
            else if(action.equalsIgnoreCase("add"))
                addCustomer();
            else if(action.equalsIgnoreCase("del") || 
                    action.equalsIgnoreCase("delete"))
                deleteCustomer();
            else if(action.equalsIgnoreCase("help") ||
                    action.equalsIgnoreCase("menu"))
                displayMenu();
            else if(action.equalsIgnoreCase("exit"))
            {
                System.out.println("Bye.");
                customerRW.disconnect();
            }
            else
                System.out.println("Error.  Unrecognized command.");
        }
        
    }
//display menu method
    public static void displayMenu()
    {
        System.out.println("MAIN MENU");
        System.out.println("list    - List all customers");
        System.out.println("add     - Add a customer");
        System.out.println("del     - Delete a customer");
        System.out.println("help    - Show this menu");
        System.out.println("exit    - Exit this application");
    }
    
    //list all customers method
    public static void displayAllCustomers()
    {
        System.out.println("CUSTOMER LIST");
        
        //create array list of customers
        ArrayList<Customer> customers = customerRW.getCustomers();
        if(customers == null)
            System.out.println("There was an error in retrieving the customers");
        else
        {
            //build a string with the customers information
            Customer c = null;
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < customers.size(); i++)
            {
                c = customers.get(i);
                sb.append(StringUtils.padWithSpaces(c.getNameFirst(),
                        FNAME_SIZE + 4));
                sb.append(StringUtils.padWithSpaces(c.getNameLast(),
                        LNAME_SIZE + 4));
                sb.append(StringUtils.padWithSpaces(c.getPhoneNumber(),
                        PHONE_NUMBER_SIZE + 4));
                sb.append(c.getEmail());
                sb.append("\n");
            }
            System.out.println(sb.toString());
        }
    }
    
    //method to add a customer
    public static void addCustomer()
    {
        //create variables which will be passed to Customer constructor
        //using the validator class
        String nameFirst = Validator.getString(
                sc, "Enter customer's first name: ");
        String nameLast = Validator.getString(
                sc, "Enter customer's last name: ");
        
        //create flag to prime the while loop for phone number.
        //create variable
        boolean isValidPhone = false;
        String phoneNumber = null;
        while(isValidPhone == false)
        {
            phoneNumber = Validator.getString(
                    sc, "Enter customer's 10 digit phone number, separated by . "
                    + "or -: ");
            if(Validator.isPhoneNumber(phoneNumber) == false)
            {
                System.out.println("That is not a valid phone number.  " +
                        "Please try again.");
            }
        }
        
        //create flag to prime the while loop for email
        boolean isValidEmail = false;
        String email = null;
        while(isValidEmail == false)
        {
            email = Validator.getString(
                    sc, "Enter customer's email address: ");
            if(Validator.isEmail(email) == false)
            {
                System.out.println("That is not a valid email.  " +
                        "Please try again.");
            }
        }
        
        //create a new Customer object
        Customer customer = new Customer();
        customer.setNameFirst(nameFirst);
        customer.setNameLast(nameLast);
        customer.setEmail(email);
        customer.setPhoneNumber(phoneNumber);
        
        //add customer to db, and check to make sure 
        boolean success = customerRW.addCustomer(customer);
        System.out.println();
        //display appropriate message
        if(success)
            System.out.println(nameFirst + " " +nameLast +
                    " was successfully added to the database");
        else
            System.out.println("There was an error in adding the customer " +
                    "to the database!");
    }
    
    //method to delete a customer
    public static void deleteCustomer()
    {
        //set a string equal to input from user
        String email = Validator.getString(sc
                , "Enter customer email to delete:");
        //retrieve the customer with the specified email
        Customer c = customerRW.getCustomer(email);
        
        System.out.println();
        //if there is a customer
        if(c != null)
        {
            //set boolean equal to delete customer
            boolean success = customerRW.deleteCustomer(c);
            //if it worked
            if(success)
                System.out.println(c.getNameFirst() + c.getNameLast() +
                        " was deleted from the database.");
            //if it didn't work
            else
                System.out.println("There was an error in processing your " +
                        "request at this time");
        }
        //if there isn't a customer with that email
        else
            System.out.println("There is no customer with that email");
        
        
    }
}


