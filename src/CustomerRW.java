/*
    Interface that extends CustomerReader and CustomerWriter
 */

/**
 *
 * @author Andrew M. Siniarski
 *          the dude abides
 */
public interface CustomerRW extends CustomerReader, CustomerWriter, EmbeddedDB
{
    //all methods and private variables from 
    //CustomerReader and CustomerWriter and EmbeddedDB
}
