/*
 *Customer class to hold all private variables and public methods
 */

/**
 *
 * @author Andrew M. Siniarski
 *          the dude abides
 */
public class Customer 
{
    //private member variables
    private String nameFirst;
    private String nameLast;
    private String phoneNumber;
    private String email;
    
    //default constructor
    public Customer()
    {
        this("", "", "", "");
    }
    //constructor with parameters
    public Customer(String nameFirst, String nameLast, String phoneNumber,
            String email)
    {
        this.nameFirst = nameFirst;
        this.nameLast = nameLast;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }
    
    //*************************************
    //set and get for all private variables
    //************************************
    
    //nameFirst
    public void setNameFirst(String nameFirst)
    {
        this.nameFirst = nameFirst;
    }
    
    public String getNameFirst()
    {
        return nameFirst;
    }
    
    //nameLast
    public void setNameLast(String nameLast)
    {
        this.nameLast = nameLast;
    }
    
    public String getNameLast()
    {
        return nameLast;
    }
    
    //phoneNumber
    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }
    
    public String getPhoneNumber()
    {
        return phoneNumber;
    }
    
    //email
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    public String getEmail()
    {
        return email;
    }
    //************************************
    //      end set/get
    //************************************
    
    //******************************
    //  override equals method
    //****************************
    
    //override equals
    public boolean equals(Object object)
    {
        if(object instanceof Customer)
        {
            Customer customer2 = (Customer) object;
            if
            (
                nameFirst.equals(customer2.getNameFirst()) &&
                nameLast.equals(customer2.getNameLast()) &&
                phoneNumber.equals(customer2.getPhoneNumber()) &&
                email.equals(customer2.getEmail())
            )
                return true;
        }
        
        return false;
    }
    
    
    
}
