/*
 * @author: Andrew M. Siniarski
        the dude abides
    @created: 2 June 15
 */

/**
 *
 * this method enables the CustomerRW interface to use 
 * the appropriate storage mechanism in CustomerDB
 */
public class MakeRW 
{
    public static CustomerRW getCustomerRW()
    {
        CustomerRW cRW = new CustomerDB();
        return cRW;
    }
}
